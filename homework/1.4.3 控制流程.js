/* 
作業一
公司交給你一個案子，要撰寫一支對網站上線上填寫表單進行檢查欄位的程式，以下是這個表單的欄位與要檢查的規則:

1. 姓名(fullname): 最多4個字元，必填
2. 手機號碼(mobile): 手機號碼必需是10個數字
3. 出生年月日(birthday): 年1970-2000，月份1-12，日期1-31
4. 住址(address): 最少8個字元，最多50個字元，必填
5. Email(email): 最少10個字元，最多50個字元，必填
請問要如何寫出每個欄位的判斷檢查的程式碼。
*/

// 1. 姓名
function checkName(name) {
    if(!name) return '姓名不可為空'
    if(name.length >= 4) return '不可超過 4 個字元' 
    return true
}

// 2. 手機號碼

function checkMobile(number) {
    number = number.toString()
    if(isNaN(number)) return '手機號碼僅限輸入數字'
    if(number.length !== 10) return '手機號碼需為 10 碼'
    return true

}

// 3. 出生年月日
// date 需包裝成物件
function checkBirth(date) {
    if(date.year < 1970 || date.year > 2000) return '年份必須在 1970 ~ 2000 年間'
    if(date.month < 1 || date.month > 12) return '月份必須在 1 - 12 月間'
    if(date.day < 1 || date.day > 31) return '日期必須在 1 - 31 日間'
    return true
}

// 4. 住址 

function checkAddress(address) {
    address = address.toString()
    if(!address) return '請填寫住址欄位'
    let length = address.length
    if(length < 8 || length > 50) return '住址最少8個字元，最多50個字元'
    return true
}

// 5. Email

function checkEmail(email) {
    email = email.toString()
    if(!email) return '請填寫信箱欄位'
    let length = email.length
    if(length < 10 || length > 50) return 'Email 最少10個字元，最多50個字元，必填'
    return true
}