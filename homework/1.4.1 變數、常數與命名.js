/*
作業一
學校給了你一個新的工作，希望把學生的資料的各項功能，寫成一個JavaScript的應用程式，經過調查和討論後，學生會有幾個基本的資料:

學號
學生的姓名
學生的出生年月日
學生的住址
學生的聯絡電話
學生的家長(其中一位聯絡用的)
現在要請你把這些資料的項目，寫成變數或常數的名稱，請你進行命名的工作。
*/

const ID = 123
const name = '奇跡'
const birth = '2021/02/18'
const address = '407台中市西屯區市政北二路282號'
const phoneNumber = '04-36061688'
const parent = {
    father: 'rock',
    mother: 'alysa'
}

/* 
作業二
最近公司接了一個家用遊戲代理商妥託的案子，希望能作一個網站上的JavaScript應用程式，把遊戲公司目前代理的遊戲軟體，放到網路上提供給他們的客戶了解。經過調查和討論後，遊戲大致會有以下幾個資料欄位:

遊戲的發行編號(例如: KK123)
遊戲名稱
遊戲的製作公司
遊戲的發行日期
遊戲支援的語言(例如: 日文、中文、英文...)
遊戲支援的平台(例如: PS4, Xbox...)
遊戲的種類(例如: 動作類、射擊類)
遊戲是否為18禁
遊戲是否需要網路連線
現在要請你把這些資料的項目，寫成變數或常數的名稱，請你進行命名的工作。
*/

const ID = KK123
const game = '激鬥峽谷'
const company = 'Garena'
const issueDate = '2019/02/01'
const language = '中文、日文、英文'
const platform = 'PS4, Xbox'
const type = '動作類'
const isRestricted = false
const shouldInternet = true

/*
作業三
打字練習對於提升撰寫程式碼的速度是很有幫助的。有人說很多學寫程式的人，最後只會兩個按鍵組合，一個是複製(Ctrl+C)，另一個是貼上(Ctrl+V)。
對於我們有遠大理想的未來優秀程式設計師，這樣的按鍵組合是不足的，所以需要多練習打字。
請依照以下的兩個區域，打出這些常用的關鍵字與符號了。如果你覺得太簡單，可以把行數對調再練習下一次，而且最好能練習到不看鍵盤就能打出來:
*/

/*
if else else if const let let const
import export return import export return
switch case default break case switch break
class extends super class extends super
for in do while continue while for in do
try throw function catch function try catch
if else function const let for return
*/

/*
if else else if const let let const
import export return import export return
switch case default break case switch break
class extends super class extends super
for in do while continue while for in do
try throw function catch function try catch
if else function const let for return
*/

/*
+-% {!?:} (++--) [1][0][3]
(){_-/\} (_.){}() => >=
<= == !== && ||
*/

/*
+-% {!?:} (++--) [1][0][3]
(){_-/\} (_.){}() => >=
<= == !== && ||
*/


