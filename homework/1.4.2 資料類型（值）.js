/* 
作業一
Math.random()方法是在JavaScript中產生亂數的一個方法。不論你要作野球拳遊戲，還是線上博杯拿大獎的應用程式，都可以用它來產生亂數。Math.random會隨時產生一個0到1的浮點數，它的基本用法如下:

Math.floor(Math.random() * 6) + 1
上面的語句代表要產生1到6的整數亂數
1代表開始數字
6代表可能情況有6種
所以如果是以最大值max，最小值min來寫這個語句，這也是產生整數的亂數，會像下面這樣:

Math.floor(Math.random() * (max - min + 1)) + min
現在要利用這個產生亂數的方法，作一個線上抽獎的活動，客戶說有下面幾個獎品:

50吋液晶電視 1台
PS4遊戲機 3台
手機行動充電器 10台
7-11的100元購物券 100張
預計參與抽獎的人數有1萬人，要如何來寫這個程式的抽獎過程的應用程式？
*/

// 獎品共有 114 項，人數 10000 人。
// 給每個人抽獎卷，上面編號 0 ~ 10000，抽出的號碼即為得獎者。

function lottery() {
    return Math.floor(Math.random() * 10000) + 1
}

console.log(lottery())

/*
作業二
有一家日本的房地產公司來台灣設立新的分公司，因為日本的房地產用的計算單位與台灣的不同，所以要委託你寫一個程式，這個程式是希望能轉換平方公尺為坪數。要如何來寫這個轉換的公式？
*/

// 1 平方公尺 = 0.3025 坪
// 輸入立方公尺 輸出坪數

function doTransformUnit(num) {
    return num * 0.3025
}

console.log(doTransformUnit(10))